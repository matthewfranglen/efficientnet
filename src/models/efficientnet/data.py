from __future__ import annotations
from dataclasses import asdict, dataclass
import json
from pathlib import Path
from typing import List
from torch import nn
from torch.utils import model_zoo

_STATE_MAP = Path(__file__).absolute().parent / "parameter-map.json"


@dataclass(frozen=True)  # pylint: disable=too-many-instance-attributes
class EfficientNetParameters:
    url: str

    width_coefficient: float
    depth_coefficient: float
    image_size: int
    dropout_rate: float

    batch_norm_momentum: float = 0.99
    batch_norm_epsilon: float = 1e-3
    drop_connect_rate: float = 0.2
    num_classes: int = 1_000
    depth_divisor: int = 8
    min_depth: float = None

    def clone(self, **params) -> EfficientNetParameters:
        """ Copy and change these parameters """
        parameters = {**asdict(self), **params}
        return EfficientNetParameters(**parameters)


@dataclass(frozen=True)  # pylint: disable=too-many-instance-attributes
class EfficientNetBlockParameters:
    kernel_size: int
    num_repeat: int
    input_filters: int
    output_filters: int
    expand_ratio: int
    id_skip: bool
    stride: List[int]
    se_ratio: float

    def clone(self, **params) -> EfficientNetParameters:
        """ Copy and change these parameters """
        parameters = {**asdict(self), **params}
        return EfficientNetBlockParameters(**parameters)


EFFICIENTNET_PARAMETERS = {
    "efficientnet-b0": EfficientNetParameters(
        url="http://storage.googleapis.com/public-models/efficientnet/efficientnet-b0-355c32eb.pth",
        width_coefficient=1.0,
        depth_coefficient=1.0,
        image_size=224,
        dropout_rate=0.2,
    ),
    "efficientnet-b1": EfficientNetParameters(
        url="http://storage.googleapis.com/public-models/efficientnet/efficientnet-b1-f1951068.pth",
        width_coefficient=1.0,
        depth_coefficient=1.1,
        image_size=240,
        dropout_rate=0.2,
    ),
    "efficientnet-b2": EfficientNetParameters(
        url="http://storage.googleapis.com/public-models/efficientnet/efficientnet-b2-8bb594d6.pth",
        width_coefficient=1.1,
        depth_coefficient=1.2,
        image_size=260,
        dropout_rate=0.3,
    ),
    "efficientnet-b3": EfficientNetParameters(
        url="http://storage.googleapis.com/public-models/efficientnet/efficientnet-b3-5fb5a3c3.pth",
        width_coefficient=1.2,
        depth_coefficient=1.4,
        image_size=300,
        dropout_rate=0.3,
    ),
    "efficientnet-b4": EfficientNetParameters(
        url="http://storage.googleapis.com/public-models/efficientnet/efficientnet-b4-6ed6700e.pth",
        width_coefficient=1.4,
        depth_coefficient=1.8,
        image_size=380,
        dropout_rate=0.4,
    ),
    "efficientnet-b5": EfficientNetParameters(
        url="http://storage.googleapis.com/public-models/efficientnet/efficientnet-b5-b6417697.pth",
        width_coefficient=1.6,
        depth_coefficient=2.2,
        image_size=456,
        dropout_rate=0.4,
    ),
    "efficientnet-b6": EfficientNetParameters(
        url="http://storage.googleapis.com/public-models/efficientnet/efficientnet-b6-c76e70fd.pth",
        width_coefficient=1.8,
        depth_coefficient=2.6,
        image_size=528,
        dropout_rate=0.5,
    ),
    "efficientnet-b7": EfficientNetParameters(
        url="http://storage.googleapis.com/public-models/efficientnet/efficientnet-b7-dcc49843.pth",
        width_coefficient=2.0,
        depth_coefficient=3.1,
        image_size=600,
        dropout_rate=0.5,
    ),
}

EFFICIENTNET_BLOCK_PARAMETERS = [
    EfficientNetBlockParameters(
        kernel_size=3,
        num_repeat=1,
        input_filters=32,
        output_filters=16,
        expand_ratio=1,
        id_skip=True,
        stride=[1],
        se_ratio=0.25,
    ),
    EfficientNetBlockParameters(
        kernel_size=3,
        num_repeat=2,
        input_filters=16,
        output_filters=24,
        expand_ratio=6,
        id_skip=True,
        stride=[2],
        se_ratio=0.25,
    ),
    EfficientNetBlockParameters(
        kernel_size=5,
        num_repeat=2,
        input_filters=24,
        output_filters=40,
        expand_ratio=6,
        id_skip=True,
        stride=[2],
        se_ratio=0.25,
    ),
    EfficientNetBlockParameters(
        kernel_size=3,
        num_repeat=3,
        input_filters=40,
        output_filters=80,
        expand_ratio=6,
        id_skip=True,
        stride=[2],
        se_ratio=0.25,
    ),
    EfficientNetBlockParameters(
        kernel_size=5,
        num_repeat=3,
        input_filters=80,
        output_filters=112,
        expand_ratio=6,
        id_skip=True,
        stride=[1],
        se_ratio=0.25,
    ),
    EfficientNetBlockParameters(
        kernel_size=5,
        num_repeat=4,
        input_filters=112,
        output_filters=192,
        expand_ratio=6,
        id_skip=True,
        stride=[2],
        se_ratio=0.25,
    ),
    EfficientNetBlockParameters(
        kernel_size=3,
        num_repeat=1,
        input_filters=192,
        output_filters=320,
        expand_ratio=6,
        id_skip=True,
        stride=[1],
        se_ratio=0.25,
    ),
]


def load_pretrained_weights(model: nn.Module, model_name: str) -> None:
    """ Loads pretrained weights, and downloads if loading for the first time. """
    mapping = json.loads(_STATE_MAP.read_text())[model_name]
    state = {
        mapping[key]: value
        for key, value in model_zoo.load_url(
            EFFICIENTNET_PARAMETERS[model_name].url
        ).items()
    }
    model.load_state_dict(state)
