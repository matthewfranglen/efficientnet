# pylint: disable=no-member, arguments-differ
from functools import partial
import torch
from torch import nn
from torch.nn import functional as F

from src.models.efficientnet.data import (
    EfficientNetParameters,
    EfficientNetBlockParameters,
)
from .swish import MemoryEfficientSwish
from .conv import pad_conv_norm


def mobile_inverted_residual_bottleneck(
    block_parameters: EfficientNetBlockParameters,
    global_parameters: EfficientNetParameters,
    drop_connect_rate: float = 0,
) -> nn.Module:
    layers = []
    output_channels = block_parameters.input_filters * block_parameters.expand_ratio

    pcn = partial(
        pad_conv_norm,
        image_size=global_parameters.image_size,
        batch_norm_momentum=1 - global_parameters.batch_norm_momentum,
        batch_norm_epsilon=global_parameters.batch_norm_epsilon,
    )

    if block_parameters.expand_ratio != 1:
        layers.append(
            pcn(
                in_channels=block_parameters.input_filters,
                out_channels=output_channels,
                kernel_size=1,
                bias=False,
            )
        )

    # Depthwise convolution phase
    layers.append(
        pcn(
            in_channels=output_channels,
            out_channels=output_channels,
            groups=output_channels,  # groups makes it depthwise
            kernel_size=block_parameters.kernel_size,
            stride=block_parameters.stride,
            bias=False,
        )
    )

    # Squeeze and Excitation layer, if desired
    if block_parameters.se_ratio is not None and 0 < block_parameters.se_ratio <= 1:
        layers.append(
            SqueezeAndExcitation(
                block_parameters=block_parameters, output_channels=output_channels
            )
        )

    # Output phase
    layers.append(
        pcn(
            in_channels=output_channels,
            out_channels=block_parameters.output_filters,
            kernel_size=1,
            bias=False,
            has_swish=False,
        )
    )

    if (
        block_parameters.id_skip
        and block_parameters.stride == 1
        and block_parameters.input_filters == block_parameters.output_filters
    ):
        if drop_connect_rate:
            return SkipDropConnection(drop_connect_rate, *layers)
        return SkipConnection(*layers)
    return nn.Sequential(*layers)


class SqueezeAndExcitation(nn.Sequential):
    def __init__(
        self, block_parameters: EfficientNetBlockParameters, output_channels: int
    ):
        num_squeezed_channels = max(
            1, int(block_parameters.input_filters * block_parameters.se_ratio)
        )
        super().__init__(
            nn.AdaptiveAvgPool2d(1),
            nn.Conv2d(
                in_channels=output_channels,
                out_channels=num_squeezed_channels,
                kernel_size=1,
            ),  # squeeze
            MemoryEfficientSwish(),
            nn.Conv2d(
                in_channels=num_squeezed_channels,
                out_channels=output_channels,
                kernel_size=1,
            ),  # expand
        )

    def forward(self, _in, *others):
        squeezed = super().forward(_in, *others)
        return torch.sigmoid(squeezed) * _in


class SkipConnection(nn.Sequential):
    def forward(self, _in, *others):
        out = super().forward(_in, *others)
        return _in + out


class SkipDropConnection(nn.Sequential):
    def __init__(self, dropout: float, *layers):
        super().__init__(*layers)
        self.dropout = dropout

    def forward(self, _in, *others):
        out = super().forward(_in, *others)
        return F.dropout(_in, p=self.dropout, training=self.training) + out
