from typing import Union, Tuple
from torch import nn

from .padding import pad
from .swish import MemoryEfficientSwish


Value2d = Union[int, Tuple[int, int]]


def pad_conv_norm(  # pylint: disable=too-many-arguments
    *,
    image_size: Value2d,
    batch_norm_momentum: float,
    batch_norm_epsilon: float,
    in_channels: int,
    out_channels: int,
    kernel_size: Value2d,
    stride: Value2d = 1,
    padding: Value2d = 0,
    dilation: Value2d = 1,
    groups: int = 1,
    bias: bool = True,
    padding_mode: str = "zeros",
    has_swish: bool = True,
) -> nn.Module:
    """ A repeated pattern in efficientnet is pad -> conv2d -> batch norm -> swish.
        This wraps that in a sequential block. """
    layers = []
    conv = nn.Conv2d(
        in_channels,
        out_channels,
        kernel_size,
        stride,
        padding,
        dilation,
        groups,
        bias,
        padding_mode,
    )
    padding = pad(conv, image_size)
    if padding:
        layers.append(padding)
    layers.append(conv)
    layers.append(
        nn.BatchNorm2d(
            num_features=out_channels,
            momentum=batch_norm_momentum,
            eps=batch_norm_epsilon,
        )
    )
    if has_swish:
        layers.append(MemoryEfficientSwish())

    return nn.Sequential(*layers)
