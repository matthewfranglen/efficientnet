import math
from typing import Union, Tuple, Optional

from torch import nn
from fastai.core import listify


def pad(
    conv: nn.Conv2d, image_size: Union[int, Tuple[int, int]]
) -> Optional[nn.Module]:
    kernel_size = conv.weight.shape[-2:]
    return _padding_module(
        stride=conv.stride,
        dilation=conv.dilation,
        kernel_size=kernel_size,
        image_size=image_size,
    )


def _padding_module(  # pylint: disable=too-many-locals
    stride: Union[int, Tuple[int, int]],
    dilation: Union[int, Tuple[int, int]],
    kernel_size: Union[int, Tuple[int, int]],
    image_size: Union[int, Tuple[int, int]],
) -> Optional[nn.Module]:
    stride_height, stride_width = listify(stride, 2)
    dilation_height, dilation_width = listify(dilation, 2)
    kernel_height, kernel_width = listify(kernel_size, 2)
    image_height, image_width = listify(image_size, 2)

    output_height = math.ceil(image_height / stride_height)
    output_width = math.ceil(image_width / stride_width)

    pad_h = _calculate_padding(
        _in=image_height,
        out=output_height,
        stride=stride_height,
        dilation=dilation_height,
        kernel=kernel_height,
    )
    pad_w = _calculate_padding(
        _in=image_width,
        out=output_width,
        stride=stride_width,
        dilation=dilation_width,
        kernel=kernel_width,
    )

    if pad_h > 0 or pad_w > 0:
        return nn.ZeroPad2d(
            (pad_w // 2, pad_w - pad_w // 2, pad_h // 2, pad_h - pad_h // 2)
        )
    return None


def _calculate_padding(
    _in: int, out: int, stride: int, dilation: int, kernel: int
) -> Tuple[int, int]:
    return max((out - 1) * stride + (kernel - 1) * dilation + 1 - _in, 0)
