from __future__ import annotations
from functools import partial
import math
from typing import List

from torch import nn

from src.models.efficientnet.data import (
    EfficientNetParameters,
    EfficientNetBlockParameters,
    EFFICIENTNET_PARAMETERS,
    EFFICIENTNET_BLOCK_PARAMETERS,
    load_pretrained_weights,
)
from src.models.efficientnet.modules.conv import pad_conv_norm
from src.models.efficientnet.modules.mobile_inverted_residual_bottleneck import (
    mobile_inverted_residual_bottleneck,
)


class EfficientNet(nn.Sequential):
    def __init__(
        self,
        blocks: List[EfficientNetBlockParameters],
        global_parameters: EfficientNetParameters,
    ):
        pcn = partial(
            pad_conv_norm,
            image_size=global_parameters.image_size,
            batch_norm_momentum=1 - global_parameters.batch_norm_momentum,
            batch_norm_epsilon=global_parameters.batch_norm_epsilon,
        )

        in_channels = 3  # rgb
        out_channels = round_filters(32, global_parameters)

        layers = []
        layers.append(
            pcn(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=3,
                stride=2,
                bias=False,
            )
        )

        block_layers = []
        block_count = sum(max(1, block.num_repeat) for block in blocks)
        for block_parameters in blocks:
            # Update block input and output filters based on depth multiplier.
            block_parameters = block_parameters.clone(
                input_filters=round_filters(
                    block_parameters.input_filters, global_parameters
                ),
                output_filters=round_filters(
                    block_parameters.output_filters, global_parameters
                ),
                num_repeat=round_repeats(
                    block_parameters.num_repeat, global_parameters
                ),
            )

            # The first block needs to take care of stride and filter size increase.
            block_layers.append(
                mobile_inverted_residual_bottleneck(block_parameters, global_parameters)
            )
            if block_parameters.num_repeat > 1:
                block_parameters = block_parameters.clone(
                    input_filters=block_parameters.output_filters, stride=1
                )
            for _ in range(block_parameters.num_repeat - 1):
                if global_parameters.drop_connect_rate:
                    drop_connect_rate = (
                        global_parameters.drop_connect_rate
                        * len(block_layers)
                        / block_count
                    )
                else:
                    drop_connect_rate = 0
                block_layers.append(
                    mobile_inverted_residual_bottleneck(
                        block_parameters,
                        global_parameters,
                        drop_connect_rate=drop_connect_rate,
                    )
                )

        layers.append(nn.Sequential(*block_layers))

        in_channels = block_parameters.output_filters  # output of final block
        out_channels = round_filters(1280, global_parameters)
        layers.append(
            pcn(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=1,
                bias=False,
            )
        )
        layers.append(EfficientNetHead(out_channels, global_parameters))

        super().__init__(*layers)

    @classmethod
    def from_name(cls, name: str) -> EfficientNet:
        assert (
            name in EFFICIENTNET_PARAMETERS
        ), f"Cannot find architecture for {name}, please try one of {', '.join(sorted(EFFICIENTNET_PARAMETERS.keys()))}"

        return cls(EFFICIENTNET_BLOCK_PARAMETERS, EFFICIENTNET_PARAMETERS[name])

    @classmethod
    def from_pretrained(cls, name: str) -> EfficientNet:
        model = cls.from_name(name)
        load_pretrained_weights(model, name)
        return model


class EfficientNetHead(nn.Module):
    def __init__(self, in_channels: int, global_parameters: EfficientNetParameters):
        super().__init__()
        self.avg_pool = nn.AdaptiveAvgPool2d(1)
        self.dropout = nn.Dropout(global_parameters.dropout_rate)
        self.fc = nn.Linear(  # pylint: disable=invalid-name
            in_channels, global_parameters.num_classes
        )

    def forward(self, _in):  # pylint: disable=arguments-differ
        batch_size = _in.shape[0]
        out = self.avg_pool(_in)
        out = out.view(batch_size, -1)
        out = self.dropout(out)
        return self.fc(out)


def round_filters(filters: int, global_parameters: EfficientNetParameters) -> int:
    """ Calculate and round number of filters based on depth multiplier. """
    multiplier = global_parameters.width_coefficient
    if not multiplier:
        return filters
    divisor = global_parameters.depth_divisor
    min_depth = global_parameters.min_depth
    filters *= multiplier
    min_depth = min_depth or divisor
    new_filters = max(min_depth, int(filters + divisor / 2) // divisor * divisor)
    if new_filters < 0.9 * filters:  # prevent rounding by more than 10%
        new_filters += divisor
    return int(new_filters)


def round_repeats(repeats: int, global_parameters: EfficientNetParameters) -> int:
    """ Round number of filters based on depth multiplier. """
    multiplier = global_parameters.depth_coefficient
    if not multiplier:
        return repeats
    return int(math.ceil(multiplier * repeats))
