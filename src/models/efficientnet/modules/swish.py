# pylint: disable=no-member, arguments-differ
import torch
from torch import nn


class SwishImplementation(torch.autograd.Function):
    @staticmethod
    def forward(ctx, _in, *_):
        result = _in * torch.sigmoid(_in)
        ctx.save_for_backward(_in)
        return result

    @staticmethod
    def backward(ctx, gradient_output):
        _in = ctx.saved_variables[0]
        sigmoid_in = torch.sigmoid(_in)
        return gradient_output * (sigmoid_in * (1 + _in * (1 - sigmoid_in)))


class MemoryEfficientSwish(nn.Module):
    def forward(self, _in, *_):
        return SwishImplementation.apply(_in)


class Swish(nn.Module):
    def forward(self, _in, *_):
        return _in * torch.sigmoid(_in)
