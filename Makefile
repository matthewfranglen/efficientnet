.PHONY: clean data lint requirements sync_data_to_s3 sync_data_from_s3

#################################################################################
# GLOBALS                                                                       #
#################################################################################

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
BUCKET = [OPTIONAL] your-bucket-for-syncing-data (do not include 's3://')
PROFILE = default
PROJECT_NAME = efficientnet
PYTHON_INTERPRETER = pipenv run python

RAW_FOLDER = data/raw
RAW_TRAINING_DATA = $(RAW_FOLDER)/anno_train.csv
RAW_VALIDATION_DATA = $(RAW_FOLDER)/anno_test.csv
RAW_LABELS = $(RAW_FOLDER)/names.csv
RAW_CAR_ZIP = $(RAW_FOLDER)/car_data.zip

PROCESSED_FOLDER = data/processed
PROCESSED_TRAINING_DATA = $(PROCESSED_FOLDER)/train.csv
PROCESSED_VALIDATION_DATA = $(PROCESSED_FOLDER)/valid.csv
PROCESSED_LABELS = $(PROCESSED_FOLDER)/labels.csv
PROCESSED_IMAGES = $(PROCESSED_FOLDER)/car_data

#################################################################################
# COMMANDS                                                                      #
#################################################################################

## Set up python interpreter environment
create_environment: .make/pipenv

## Delete all compiled Python files
clean:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete

## Test python environment is setup correctly
test_environment: create_environment
	$(PYTHON_INTERPRETER) test_environment.py

## Run python tests
test: create_environment
	pipenv run pytest tests

## Make Dataset (146M)
data: create_environment $(PROCESSED_TRAINING_DATA) $(PROCESSED_VALIDATION_DATA) $(PROCESSED_LABELS) $(PROCESSED_IMAGES)

$(PROCESSED_TRAINING_DATA): $(RAW_TRAINING_DATA)
	cp $(RAW_TRAINING_DATA) $(PROCESSED_TRAINING_DATA)

$(PROCESSED_VALIDATION_DATA): $(RAW_VALIDATION_DATA)
	cp $(RAW_VALIDATION_DATA) $(PROCESSED_VALIDATION_DATA)

$(PROCESSED_LABELS): $(RAW_LABELS)
	cp $(RAW_LABELS) $(PROCESSED_LABELS)

$(PROCESSED_IMAGES): $(RAW_CAR_ZIP)
	if [ ! -e $(PROCESSED_IMAGES) ]; then unzip $(RAW_CAR_ZIP) -d $(PROCESSED_FOLDER); fi

$(RAW_TRAINING_DATA): download
$(RAW_VALIDATION_DATA): download
$(RAW_LABELS): download
download: $(RAW_CAR_ZIP)

$(RAW_CAR_ZIP):
	pipenv run kaggle datasets download --path data/raw --unzip jutrera/stanford-car-dataset-by-classes-folder

## Lint python code
lint: create_environment
	pipenv run pylint src

.make/pipenv: Pipfile
	pipenv install --dev
	if [ ! -e .make ]; then mkdir .make; fi
	touch .make/pipenv

#################################################################################
# PROJECT RULES                                                                 #
#################################################################################



#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := help

# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
.PHONY: help
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')
