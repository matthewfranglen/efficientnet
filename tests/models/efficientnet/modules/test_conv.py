# pylint: disable=no-member
from typing import Tuple

import pytest
import torch
from torch import nn

from src.models.efficientnet.original.impl import (
    Conv2dStaticSamePadding as OriginalPadding,
)
from src.models.efficientnet.modules.conv import pad_conv_norm


def test_fast_equivalent_behaviour():
    in_channels = 3
    out_channels = 32
    groups = 1
    bias = True
    kernel_size = 3
    stride = 2
    dilation = 1
    image_size = 224

    old, new = _make(
        in_channels=in_channels,
        out_channels=out_channels,
        kernel_size=kernel_size,
        stride=stride,
        dilation=dilation,
        groups=groups,
        bias=bias,
        image_size=image_size,
    )
    tensor = torch.rand(4, in_channels, image_size, image_size)
    with torch.no_grad():
        expected = old(tensor.clone().detach())
        actual = new(tensor)
    assert torch.all(expected.eq(actual))


@pytest.mark.slow
@pytest.mark.parametrize("kernel_size", range(1, 7))
@pytest.mark.parametrize("stride", range(1, 7))
@pytest.mark.parametrize("dilation", range(1, 7))
@pytest.mark.parametrize("image_size", range(100, 600, 25))
def test_equivalent_behaviour(kernel_size, stride, dilation, image_size):
    in_channels = 3
    out_channels = 32
    groups = 1
    bias = True

    old, new = _make(
        in_channels=in_channels,
        out_channels=out_channels,
        kernel_size=kernel_size,
        stride=stride,
        dilation=dilation,
        groups=groups,
        bias=bias,
        image_size=image_size,
    )
    tensor = torch.rand(4, in_channels, image_size, image_size)
    with torch.no_grad():
        expected = old(tensor.clone().detach())
        actual = new(tensor)
    assert torch.all(expected.eq(actual))


def _make(  # pylint: disable=too-many-arguments
    in_channels: int,
    out_channels: int,
    kernel_size: int,
    stride: int,
    dilation: int,
    groups: int,
    bias: bool,
    image_size: int,
) -> Tuple[nn.Module, nn.Module]:
    old = OriginalPadding(
        in_channels=in_channels,
        out_channels=out_channels,
        kernel_size=kernel_size,
        stride=stride,
        dilation=dilation,
        groups=groups,
        bias=bias,
        image_size=image_size,
    )
    new = pad_conv_norm(
        image_size=image_size,
        batch_norm_momentum=0.01,
        batch_norm_epsilon=1e-3,
        in_channels=in_channels,
        out_channels=out_channels,
        kernel_size=kernel_size,
        stride=stride,
        dilation=dilation,
        groups=groups,
        bias=bias,
        has_swish=False,
    )
    del new[-1]  # not testing the batch norm stuff
    new_conv = new[-1]

    # weight and bias are randomly initialized, need to make them the same between versions
    if bias:
        p_bias = old.bias
        new_conv.bias = nn.Parameter(p_bias.clone().detach())
    p_weight = old.weight
    new_conv.weight = nn.Parameter(p_weight.clone().detach())

    return old, new
