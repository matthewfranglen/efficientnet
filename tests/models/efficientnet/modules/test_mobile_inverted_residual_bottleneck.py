# pylint: disable=no-member
from typing import Iterator

import pytest
import torch
from torch import nn

from src.models.efficientnet.original.impl import (
    MBConvBlock as OriginalBlock,
    get_model_params as original_parameters,
)
from src.models.efficientnet.data import (
    EFFICIENTNET_PARAMETERS,
    EFFICIENTNET_BLOCK_PARAMETERS,
)
from src.models.efficientnet.modules.mobile_inverted_residual_bottleneck import (
    mobile_inverted_residual_bottleneck,
)


def test_fast_equivalent_behaviour():
    old_block_parameters, old_global_parameters = original_parameters(
        "efficientnet-b0", None
    )
    image_size = old_global_parameters.image_size
    in_channels = old_block_parameters[0].input_filters

    new_block_parameters, new_global_parameters = (
        EFFICIENTNET_BLOCK_PARAMETERS,
        EFFICIENTNET_PARAMETERS["efficientnet-b0"],
    )

    old = OriginalBlock(old_block_parameters[0], old_global_parameters)
    new = mobile_inverted_residual_bottleneck(
        new_block_parameters[0], new_global_parameters
    )
    copy_parameters(old, new)

    tensor = torch.rand(4, in_channels, image_size, image_size)
    with torch.no_grad():
        expected = old(tensor.clone().detach())
        actual = new(tensor)

    assert torch.all(expected.eq(actual))


@pytest.mark.slow
@pytest.mark.parametrize(
    "model_name",
    ["efficientnet-b0", "efficientnet-b1", "efficientnet-b2", "efficientnet-b3"],
)
def test_equivalent_behaviour(model_name):
    old_block_parameters, old_global_parameters = original_parameters(model_name, None)
    image_size = old_global_parameters.image_size
    new_block_parameters, new_global_parameters = (
        EFFICIENTNET_BLOCK_PARAMETERS,
        EFFICIENTNET_PARAMETERS[model_name],
    )

    for old_block_params, new_block_params in zip(
        old_block_parameters, new_block_parameters
    ):
        in_channels = old_block_params.input_filters
        old = OriginalBlock(old_block_params, old_global_parameters)
        new = mobile_inverted_residual_bottleneck(
            new_block_params, new_global_parameters
        )
        copy_parameters(old, new)

        tensor = torch.rand(4, in_channels, image_size, image_size)
        with torch.no_grad():
            expected = old(tensor.clone().detach())
            actual = new(tensor)

        assert torch.all(expected.eq(actual))


def copy_parameters(left: nn.Module, right: nn.Module) -> None:
    left_iter = traverse(left)
    right_iter = traverse(right)

    for left_m, right_m in zip(left_iter, right_iter):
        if getattr(left_m, "weight", None) is not None:
            right_m.weight = nn.Parameter(left_m.weight.clone().detach())
        if getattr(left_m, "bias", None) is not None:
            right_m.bias = nn.Parameter(left_m.bias.clone().detach())


def traverse(module: nn.Module) -> Iterator[nn.Module]:
    if (
        getattr(module, "weight", None) is not None
        or getattr(module, "bias", None) is not None
    ):
        yield module
    for child in module.children():
        yield from traverse(child)
