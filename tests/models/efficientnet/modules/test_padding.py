# pylint: disable=no-member
from typing import Tuple

import pytest
from torch import nn

from src.models.efficientnet.original.impl import (
    Conv2dStaticSamePadding as OriginalPadding,
    Identity,
)
from src.models.efficientnet.modules.padding import pad


@pytest.mark.parametrize("kernel_size", range(1, 7))
@pytest.mark.parametrize("stride", range(1, 7))
@pytest.mark.parametrize("dilation", range(1, 7))
@pytest.mark.parametrize("image_size", range(100, 600, 25))
def test_equivalent_padding(kernel_size, stride, dilation, image_size):
    in_channels = 3
    out_channels = 32
    groups = 1
    bias = True

    old, new = _make(
        in_channels=in_channels,
        out_channels=out_channels,
        kernel_size=kernel_size,
        stride=stride,
        dilation=dilation,
        groups=groups,
        bias=bias,
        image_size=image_size,
    )

    if isinstance(old, Identity):
        assert new is None
    else:
        assert old.padding == new.padding
        assert old.value == new.value


def _make(  # pylint: disable=too-many-arguments
    in_channels: int,
    out_channels: int,
    kernel_size: int,
    stride: int,
    dilation: int,
    groups: int,
    bias: bool,
    image_size: int,
) -> Tuple[nn.Module, nn.Module]:
    old = OriginalPadding(
        in_channels=in_channels,
        out_channels=out_channels,
        kernel_size=kernel_size,
        stride=stride,
        dilation=dilation,
        groups=groups,
        bias=bias,
        image_size=image_size,
    )
    new_conv = nn.Conv2d(
        in_channels=in_channels,
        out_channels=out_channels,
        kernel_size=kernel_size,
        stride=stride,
        dilation=dilation,
        groups=groups,
        bias=bias,
    )
    new = pad(conv=new_conv, image_size=image_size)

    return old.static_padding, new
