# pylint: disable=no-member
from typing import Iterator

import pytest
import torch
from torch import nn

from src.models.efficientnet.original.impl import (
    EfficientNet as OriginalNet,
    get_model_params as original_parameters,
)
from src.models.efficientnet.data import (
    EFFICIENTNET_PARAMETERS,
    EFFICIENTNET_BLOCK_PARAMETERS,
)
from src.models.efficientnet.modules.efficientnet import EfficientNet


@pytest.mark.parametrize(
    "model_name",
    ["efficientnet-b0", "efficientnet-b1", "efficientnet-b2", "efficientnet-b3"],
)
def test_pretrained_equivalent_behaviour(model_name):
    image_size = EFFICIENTNET_PARAMETERS[model_name].image_size
    old = OriginalNet.from_pretrained(model_name).eval()
    new = EfficientNet.from_pretrained(model_name).eval()
    copy_parameters(old, new)

    tensor = torch.rand(4, 3, image_size, image_size)
    with torch.no_grad():
        expected = old(tensor.clone().detach())
        actual = new(tensor)

    assert torch.all(expected.eq(actual))


@pytest.mark.parametrize(
    "model_name",
    ["efficientnet-b0", "efficientnet-b1", "efficientnet-b2", "efficientnet-b3"],
)
def test_equivalent_behaviour(model_name):
    old_block_parameters, old_global_parameters = original_parameters(model_name, None)
    new_block_parameters, new_global_parameters = (
        EFFICIENTNET_BLOCK_PARAMETERS,
        EFFICIENTNET_PARAMETERS[model_name],
    )

    old = OriginalNet(old_block_parameters, old_global_parameters).eval()
    new = EfficientNet(new_block_parameters, new_global_parameters).eval()
    copy_parameters(old, new)

    tensor = torch.rand(
        4, 3, new_global_parameters.image_size, new_global_parameters.image_size
    )
    with torch.no_grad():
        expected = old(tensor.clone().detach())
        actual = new(tensor)

    assert torch.all(expected.eq(actual))


def copy_parameters(left: nn.Module, right: nn.Module) -> None:
    left_iter = traverse(left)
    right_iter = traverse(right)

    for left_m, right_m in zip(left_iter, right_iter):
        if getattr(left_m, "weight", None) is not None:
            right_m.weight = nn.Parameter(left_m.weight.clone().detach())
        if getattr(left_m, "bias", None) is not None:
            right_m.bias = nn.Parameter(left_m.bias.clone().detach())


def traverse(module: nn.Module) -> Iterator[nn.Module]:
    if (
        getattr(module, "weight", None) is not None
        or getattr(module, "bias", None) is not None
    ):
        yield module
    for child in module.children():
        yield from traverse(child)
